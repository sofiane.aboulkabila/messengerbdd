1) A la racine:
- npm install
- docker compose up

2) Dans le dossier react:
- npm install
- npm run dev

3) Dans le dossier socketIO:
- npm install
- npm start

La DB est sur mongo Atlas donc vous pouvez directement vous connecter avec:

email: admin@admin.fr
password: admin123

Les imports de la DB sont dans docker-compose.yml et dans authentication-service/index !