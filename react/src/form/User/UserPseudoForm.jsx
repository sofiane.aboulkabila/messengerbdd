import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';
import axios from 'axios';
import { useAuth } from '../../security/Auth';
import secureLocalStorage from 'react-secure-storage';

export const UserPseudoForm = ({ onError }) => {
    const { userDetails } = useAuth();

    const registerSchema = Yup.object().shape({
        username: Yup.string().required('Le nom d\'utilisateur est requis.'),
    });

    return (
        <Formik
            initialValues={{ username: userDetails.username }}
            enableReinitialize
            validationSchema={registerSchema}
            onSubmit={async (values, { setSubmitting }) => {
                try {
                    const reponse = await axios.post('vide', values);

                    const { user } = reponse.data;
                    if (user) {
                        secureLocalStorage.removeItem('userDetails');

                        const userDetails = {
                            _id: user._id,
                            username: user.username,
                            email: user.email,
                            status: user.status,
                            password: user.password
                        };

                        secureLocalStorage.setItem('userDetails', JSON.stringify(userDetails));
                    }

                } catch (error) {
                    onError();
                } finally {
                    setSubmitting(false);
                }
            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '10px 5px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '10px'
                        }}>
                        Changer le pseudo de l'utilisateur: <span style={{ backgroundColor: 'gray', borderRadius: '5px', padding: '0 10px 5px 10px' }}>{userDetails.username}</span>
                    </Typography>


                    <FormControl sx={{ marginBottom: '10px' }}>
                        <FormLabel>Pseudo</FormLabel>
                        <Field as={Input} name="username" type="text" placeholder="Votre pseudo" />
                        <ErrorMessage name="username" component="div" className="errorInput" />
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2, backgroundColor: 'blue', ":hover": { backgroundColor: 'darkblue' } }}>
                        Modifier le pseudo
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
