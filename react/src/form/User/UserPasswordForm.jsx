import { useState } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';
import axios from 'axios';
import { useAuth } from '../../security/Auth';
import secureLocalStorage from 'react-secure-storage';

export const UserPasswordForm = ({ onError }) => {
    const { userDetails } = useAuth();
    const [initialValues, setInitialValues] = useState({ oldPassword: '', newPassword: '', repeatNewPassword: '' });

    const registerSchema = Yup.object().shape({
        password: Yup.string().min(6).required('Le mot de passe est requis.'),
    });

    return (
        <Formik
            initialValues={initialValues}
            enableReinitialize
            validationSchema={registerSchema}
            onSubmit={async (values, { setSubmitting }) => {
                try {
                    const reponse = await axios.post('vide', values);

                    const { user } = reponse.data;
                    if (user) {
                        secureLocalStorage.removeItem('userDetails');

                        const userDetails = {
                            _id: user._id,
                            username: user.username,
                            email: user.email,
                            status: user.status,
                            password: user.password
                        };

                        secureLocalStorage.setItem('userDetails', JSON.stringify(userDetails));
                    }

                } catch (error) {
                    onError();
                } finally {
                    setSubmitting(false);
                }
            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '10px 5px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '10px'
                        }}>
                        Changer le mot de passe de: <span style={{ backgroundColor: 'grey', borderRadius: '5px', padding: '0 10px 5px 10px' }}>{userDetails.username}</span>
                    </Typography>


                    <FormControl sx={{ marginBottom: '10px' }}>
                        <FormLabel>Ancien mot de passe</FormLabel>
                        <Field as={Input} name="oldPassword" type="text" placeholder="Votre ancien mot de passe" />
                        <ErrorMessage name="oldPassword" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl>
                        <FormLabel>Nouveau mot de passe</FormLabel>
                        <Field as={Input} name="newPassword" type="text" placeholder="Votre nouveau mot de passe" />
                        <ErrorMessage name="newPassword" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl>
                        <FormLabel>Confirmation du mot de passe</FormLabel>
                        <Field as={Input} name="repeatNewPassword" type="text" placeholder="Répétez votre nouveau mot de passe" />
                        <ErrorMessage name="repeatNewPassword" component="div" className="errorInput" />
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2, backgroundColor: 'blue', ":hover": { backgroundColor: 'darkblue' } }}>
                        Modifier le mot de passe
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
