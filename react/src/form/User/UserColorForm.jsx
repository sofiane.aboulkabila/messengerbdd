import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';
import axios from 'axios';
import { useAuth } from '../../security/Auth';
import secureLocalStorage from 'react-secure-storage';
import Box from '@mui/material/Box';

export const UserColorForm = ({ onError }) => {
    const { userDetails } = useAuth();

    const registerSchema = Yup.object().shape({
        color: Yup.string(),
        background: Yup.string()
    });

    return (
        <Formik
            initialValues={{
                color: userDetails.color || '',
                background: userDetails.background || ''
            }}
            enableReinitialize
            validationSchema={registerSchema}
            onSubmit={async (values, { setSubmitting }) => {
                try {
                    const response = await axios.post('vide', values);

                    const { user } = response.data;
                    if (user) {
                        secureLocalStorage.removeItem('userDetails');

                        const userDetails = {
                            _id: user._id,
                            username: user.username,
                            email: user.email,
                            status: user.status,
                            password: user.password,
                            color: values.color,
                            background: values.background
                        };

                        secureLocalStorage.setItem('userDetails', JSON.stringify(userDetails));
                    }

                } catch (error) {
                    onError();
                } finally {
                    setSubmitting(false);
                }
            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '10px 5px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '10px'
                        }}>
                        Changer les couleurs du pseudo de: <span style={{ backgroundColor: 'gray', borderRadius: '5px', padding: '0 10px 5px 10px' }}>{userDetails.username}</span>
                    </Typography>

                    <Box >
                        <FormControl >
                            <FormLabel>Couleur</FormLabel>
                            <Field as={Input} name="color" type="color" placeholder="Votre couleur" sx={{ display: 'flex', alignItems: 'center' }} />
                            <ErrorMessage name="color" component="div" className="errorInput" />
                        </FormControl>

                        <FormControl >
                            <FormLabel>Arrière-plan</FormLabel>
                            <Field as={Input} name="background" type="color" placeholder="Votre arrière-plan" sx={{ display: 'flex', alignItems: 'center' }} />
                            <ErrorMessage name="background" component="div" className="errorInput" />
                        </FormControl>
                    </Box>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2, backgroundColor: 'blue', ":hover": { backgroundColor: 'darkblue' } }}>
                        Modifier les couleurs du pseudo
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
