import { Formik, Field, Form, ErrorMessage } from 'formik';
import { useEffect, useState } from 'react';
import secureLocalStorage from 'react-secure-storage';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';
import axios from 'axios';

export const LoginForm = ({ onError }) => {
    const [formKey, setFormKey] = useState(Date.now());
    const [initialValues, setInitialValues] = useState({ email: '', password: '', rememberMe: false });

    useEffect(() => {
        const rememberMeValue = secureLocalStorage.getItem('rememberMe') === 'true';
        const email = secureLocalStorage.getItem('email') || '';
        const password = secureLocalStorage.getItem('password') || '';

        setInitialValues({ email, password, rememberMe: rememberMeValue });
        setFormKey(Date.now());
    }, []);

    const registerSchema = Yup.object().shape({
        email: Yup.string().email("L'adresse email est invalide.").required('Une adresse email valide est requise.'),
        password: Yup.string().min(6, 'Le mot de passe doit contenir au moins 6 caractères.').required('Un mot de passe valide est nécessaire.'),
    });

    return (
        <Formik
            key={formKey}
            initialValues={initialValues}
            enableReinitialize
            validationSchema={registerSchema}
            onSubmit={async (values, { setSubmitting }) => {
                try {
                    const response = await axios.post('http://localhost:3000/login', values);

                    const { user } = response.data;

                    if (user) {
                        const userDetails = {
                            _id: user._id,
                            username: user.username,
                            email: user.email,
                            status: user.status,
                            password: user.password
                        };

                        secureLocalStorage.setItem('token', response.data.token);
                        secureLocalStorage.setItem('userDetails', JSON.stringify(userDetails));

                        if (values.rememberMe) {
                            secureLocalStorage.setItem('rememberMe', values.rememberMe.toString());
                            secureLocalStorage.setItem('email', values.email);
                            secureLocalStorage.setItem('password', values.password);
                        } else {
                            secureLocalStorage.removeItem('rememberMe');
                            secureLocalStorage.removeItem('email');
                            secureLocalStorage.setItem('password');
                        }

                        window.location.assign('/user');
                    }
                } catch (error) {
                    onError();
                } finally {
                    setSubmitting(false);
                }
            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center', padding: '10px 15px' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '10px 5px',
                            backgroundColor: 'blue',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '10px'
                        }}>
                        Connexion
                    </Typography>

                    <FormControl sx={{ marginBottom: '10px' }}>
                        <FormLabel>Email</FormLabel>
                        <Field as={Input} name="email" type="email" placeholder="Votre email" />
                        <ErrorMessage name="email" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '10px' }}>
                        <FormLabel>Mot de passe</FormLabel>
                        <Field as={Input} name="password" type="password" placeholder="Votre mot de passe" />
                        <ErrorMessage name="password" component="div" className="errorInput" />
                    </FormControl>


                    <FormControl sx={{ marginBottom: '10px', display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                        <Field type="checkbox" name="rememberMe" />
                        <Typography sx={{ marginLeft: '10px' }}>Se souvenir de moi</Typography>
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2, backgroundColor: 'blue' }}>
                        Se connecter
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
