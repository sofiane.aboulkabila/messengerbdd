import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/joy/Button';
import Typography from '@mui/joy/Typography';
import FormControl from '@mui/joy/FormControl';
import FormLabel from '@mui/joy/FormLabel';
import Input from '@mui/joy/Input';
import axios from 'axios';

export const RegisterForm = ({ onError, onSuccess }) => {
    const initialValues = {
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
    };

    const registerSchema = Yup.object().shape({
        username: Yup.string().required('Le nom d\'utilisateur est requis.'),
        email: Yup.string().email("L'adresse email est invalide.").required('Une adresse email valide est requise.'),
        password: Yup.string().min(6, 'Le mot de passe doit contenir au moins 6 caractères.').required('Un mot de passe valide est nécessaire.'),
        confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Les mots de passe doivent correspondre.').required('La confirmation du mot de passe est requise.')
    });

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={registerSchema}
            onSubmit={async (values, { setSubmitting }) => {
                try {
                    const response = await axios.post('http://localhost:3000/signup', values);

                    console.log(response);
                    onSuccess();
                } catch (error) {
                    console.error(error);
                    onError();
                }
                setSubmitting(false);
            }}
        >
            {({ isSubmitting }) => (
                <Form style={{ textAlign: 'center' }}>
                    <Typography
                        sx={{
                            textAlign: 'center',
                            padding: '10px 5px',
                            backgroundColor: 'purple',
                            fontSize: '1.2em',
                            color: 'white',
                            borderRadius: '10px',
                            marginBottom: '10px'
                        }}>
                        Inscription
                    </Typography>

                    <FormControl sx={{ marginBottom: '10px' }}>
                        <FormLabel>Nom d'utilisateur</FormLabel>
                        <Field as={Input} name="username" type="text" placeholder="Votre nom d'utilisateur" />
                        <ErrorMessage name="username" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '10px' }}>
                        <FormLabel>Email</FormLabel>
                        <Field as={Input} name="email" type="email" placeholder="Votre email" />
                        <ErrorMessage name="email" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl sx={{ marginBottom: '10px' }}>
                        <FormLabel>Mot de passe</FormLabel>
                        <Field as={Input} name="password" type="password" placeholder="Votre mot de passe" />
                        <ErrorMessage name="password" component="div" className="errorInput" />
                    </FormControl>

                    <FormControl>
                        <FormLabel>Confirmation du mot de passe</FormLabel>
                        <Field as={Input} name="confirmPassword" type="password" placeholder="Confirmez votre mot de passe" />
                        <ErrorMessage name="confirmPassword" component="div" className="errorInput" />
                    </FormControl>

                    <Button type="submit" disabled={isSubmitting} sx={{ mt: 2, backgroundColor: 'purple' }}>
                        S'inscrire
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
