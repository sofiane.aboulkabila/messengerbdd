import { useEffect } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import { useAuth } from './Auth';

export const CheckIfNotLoggedIn = ({ children }) => {
    const { isLoggedIn, loading } = useAuth();
    const navigate = useNavigate();

    useEffect(() => {
        if (!loading && isLoggedIn) {
            navigate('/');
        }
    }, [isLoggedIn, loading, navigate]);

    if (loading) {
        return <div>...</div>;
    }

    return !isLoggedIn ? children : <Navigate to="/" />;
};