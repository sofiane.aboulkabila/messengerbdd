import { createContext, useContext, useState, useEffect } from 'react';
import secureLocalStorage from 'react-secure-storage';

const AuthContext = createContext(null);

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [loading, setLoading] = useState(true);
    const [userDetails, setUserDetails] = useState({});

    const checkLoggedIn = () => {
        const token = secureLocalStorage.getItem('token');
        const details = JSON.parse(secureLocalStorage.getItem('userDetails') || '{}');
        setIsLoggedIn(token !== null);
        setUserDetails(details);
        setLoading(false);
    };

    const login = (token, details) => {
        secureLocalStorage.setItem('token', token);
        secureLocalStorage.setItem('userDetails', JSON.stringify(details));
        setIsLoggedIn(true);
        setUserDetails(details);
    };

    const logout = () => {
        secureLocalStorage.removeItem('token');
        secureLocalStorage.removeItem('userDetails');
        setIsLoggedIn(false);
        setUserDetails({});
    };

    useEffect(() => {
        checkLoggedIn();
        window.addEventListener('storage', checkLoggedIn);

        return () => {
            window.removeEventListener('storage', checkLoggedIn);
        };
    }, []);

    return (
        <AuthContext.Provider value={{ isLoggedIn, login, logout, loading, userDetails }}>
            {children}
        </AuthContext.Provider>
    );
};
