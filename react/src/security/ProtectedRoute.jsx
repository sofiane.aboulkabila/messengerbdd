import { Navigate } from 'react-router-dom';
import { useAuth } from './Auth';

export const ProtectedRoute = ({ children }) => {
    const { isLoggedIn, loading } = useAuth();

    if (loading) {
        return <div>...</div>;
    }

    return isLoggedIn ? children : <Navigate to="/login" />;
};