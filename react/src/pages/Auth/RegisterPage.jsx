import { useState } from 'react';
import Sheet from '@mui/joy/Sheet';
import { Alert } from '@mui/material';
import { RegisterForm } from '../../form/RegisterForm';

export const RegisterPage = () => {
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);

    const handleError = () => {
        setError(true);
        setSuccess(false);
    };

    const handleSuccess = () => {
        setSuccess(true);
        setError(false);
    };

    return (
        <main>
            <div className="RegisterPage">
                <Sheet
                    sx={{
                        width: 350,
                        mx: 'auto',
                        my: 4,
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 2,
                        borderRadius: 'sm',
                        boxShadow: 'md',
                    }}
                    variant="outlined"
                >
                    {error && <Alert severity="error">Une erreur s'est produite lors de l'inscription.</Alert>}
                    {success && <Alert severity="success">Inscription réussie!</Alert>}

                    <RegisterForm onError={handleError} onSuccess={handleSuccess} />
                </Sheet>
            </div>
        </main>
    );
};
