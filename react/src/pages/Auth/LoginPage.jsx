import { useState } from 'react';
import Sheet from '@mui/joy/Sheet';
import { Alert } from '@mui/material';
import { LoginForm } from '../../form/LoginForm';

export const LoginPage = () => {
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);

    const handleError = () => {
        setError(true);
        setSuccess(false);
    };

    return (
        <main>
            <div className="LoginPage">
                <Sheet
                    sx={{
                        width: 350,
                        mx: 'auto',
                        my: 4,
                        display: 'flex',
                        flexDirection: 'column',
                        gap: 2,
                        borderRadius: 'sm',
                        boxShadow: 'md',
                    }}
                    variant="outlined"
                >
                    {error && <Alert severity="error">Une erreur s'est produite lors de la connexion.</Alert>}
                    {success && <Alert severity="success">Connexion réussie!</Alert>}

                    <LoginForm onError={handleError} />
                </Sheet>
            </div>
        </main>
    );
};
