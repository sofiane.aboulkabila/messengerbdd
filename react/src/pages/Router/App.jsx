import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { ShopPage } from '../ShopPage';
import { TchatPage } from '../TchatPage';
import { UserPage } from '../UserPage';
import { LoginPage } from '../Auth/LoginPage';
import { RegisterPage } from '../Auth/RegisterPage';
import { ErrorPage } from '../../components/ErrorPage';
import { Navbar } from '../../components/Navbar';
import { CheckIfNotLoggedIn } from '../../security/CheckIfNotLoggedIn';
import { ProtectedRoute } from '../../security/ProtectedRoute';





export const App = () => {
  return (
    <BrowserRouter>
      <div className="AppRoutes">
        <Navbar />
        <Routes>
          <Route path='/shop' element={<ShopPage />} />
          <Route path='/' element={<TchatPage />} />
          <Route path='/*' element={<ErrorPage />} />
          <Route path='/login'
            element={<CheckIfNotLoggedIn><LoginPage /></CheckIfNotLoggedIn>}
          />
          <Route path='/register'
            element={<CheckIfNotLoggedIn><RegisterPage /></CheckIfNotLoggedIn>}
          />
          <Route path='/user' element={
            <ProtectedRoute><UserPage /></ProtectedRoute>}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
};
