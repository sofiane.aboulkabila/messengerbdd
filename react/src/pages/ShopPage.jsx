import { Box, Typography } from '@mui/material';
import { ShopCard } from '../components/ShopCard';


export const ShopPage = () => {
    return (
        <Box className="ShopPage">
            <Typography className="titre" variant="h3">
                Achat de fonctionnalités prémium
            </Typography>
            <ShopCard
                title="COLORATION DE PSEUDO"
                description="Permet de sélectionner une couleur de pseudo personnalisée au sein du chat en ligne"
                price="2.99€"
                className="d1"
            />
            <ShopCard
                title="+ SALONS PRIVÉS"
                description="Permet la création de plus de canaux de discussion privés"
                price="4.99€"
                className="d2"
            />
            <ShopCard
                title="DÉBANISSEMENT"
                description="Permet de débloquer un banissement"
                price="9.99€"
                className="d3"
            />
        </Box>
    );
};
