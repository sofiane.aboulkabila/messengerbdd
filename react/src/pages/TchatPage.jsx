import { useState, useEffect, useRef } from 'react';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import io from 'socket.io-client';
import { useAuth } from '../security/Auth';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Switch from '@mui/material/Switch';

export const TchatPage = () => {
    const { userDetails, isLoggedIn } = useAuth();
    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState('');
    const [socket, setSocket] = useState(null);
    const [connectedUsers, setConnectedUsers] = useState([]);
    const [currentRoom, setCurrentRoom] = useState('general');
    const [scrollEnabled, setScrollEnabled] = useState(true);
    const messagesEndRef = useRef(null);


    useEffect(() => {
        const newSocket = io('http://localhost:3003');

        newSocket.on('connect', () => {
            newSocket.emit('send-nickname', userDetails.username);
        });

        newSocket.on('receive_message', (message) => {
            setMessages(prevMessages => [...prevMessages, message]);
        });

        newSocket.on('connected_users', (users) => {
            setConnectedUsers(users);
        });

        newSocket.on('receive_private_message', (message) => {
            setMessages(prevMessages => [...prevMessages, message]);
        });

        setSocket(newSocket);

        return () => {
            newSocket.off('receive_message');
            newSocket.off('connected_users');
            newSocket.disconnect();
        };

    }, [userDetails.username]);


    useEffect(() => {
        if (scrollEnabled) {
            messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' });
        }
    }, [messages, scrollEnabled]);

    const toggleScroll = () => {
        setScrollEnabled(!scrollEnabled);
    };
    const sendMessage = () => {
        if (isMessageValid() && userDetails && userDetails.username) {
            const isPrivate = isNewMessagePrivate();
            const recipient = isPrivate ? extractRecipient() : null;
            const messageContent = extractMessageContent();

            if (isPrivate) {
                const newPrivateMessage = {
                    sender: userDetails.username,
                    content: messageContent,
                    private: true
                };
                setMessages(prevMessages => [...prevMessages, newPrivateMessage]);

                sendPrivateMessage(messageContent, recipient);
            } else {
                sendMessageToRoom(messageContent);
            }
            resetNewMessage();
        }
    };


    const isMessageValid = () => {
        return newMessage.trim() !== '';
    };

    const isNewMessagePrivate = () => {
        return newMessage.startsWith('@');
    };

    const extractRecipient = () => {
        return newMessage.split(' ')[0].substring(1);
    };

    const extractMessageContent = () => {
        return isNewMessagePrivate() ? newMessage.substring(newMessage.indexOf(' ') + 1) : newMessage;
    };


    const sendPrivateMessage = (messageContent, recipient) => {
        socket.emit('send_private_message', {
            message: messageContent,
            recipient: recipient,
            sender: userDetails.username,
            private: true
        });
    };

    const sendMessageToRoom = (messageContent) => {
        socket.emit('send_message', {
            message: messageContent,
            room: currentRoom,
            sender: userDetails.username,
            private: false
        });
    };

    const resetNewMessage = () => {
        setNewMessage('');
    };





    const joinRoom = (roomName) => {
        if (currentRoom !== roomName) {
            socket.emit('join_room', roomName);
            setCurrentRoom(roomName);
        }
    };

    const openPrivateChat = (recipient) => {
        setNewMessage(`@${recipient} `);
    };

    const getRandomColor = () => {
        return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
    };


    const renderMessages = () => {
        return messages.map((message, index) => (
            <Box
                key={index}
                display="flex"
                flexDirection="column"
                alignItems={message.sender === userDetails.username ? 'flex-end' : 'flex-start'}
                mb={1}
            >
                <Box
                    bgcolor={message.sender === userDetails.username ? '#2196f3' : '#e0e0e0'}
                    color={message.sender === userDetails.username ? '#ffffff' : '#000000'}
                    p={1}
                    borderRadius={5}
                    boxShadow={3}
                    maxWidth="70%"
                    sx={{ paddingLeft: 2, paddingRight: 2 }}
                >
                    {message.private && (
                        <Box
                            sx={{
                                display: 'flex',
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                                backgroundColor: 'yellow',
                                color: 'red',
                                fontWeight: 'bold',
                                padding: '2px 4px',
                                borderRadius: '4px',
                                marginBottom: '4px',
                            }}
                        >
                            Message privé
                        </Box>
                    )}
                    <Box sx={{ display: 'flex' }}>
                        <AccountCircleIcon sx={{ fontSize: 32 }} onClick={() => openPrivateChat(message.sender)} />

                        <Box sx={{ display: 'inline-block', borderRadius: 5, p: 1, bgcolor: message.sender === userDetails.username ? '#2196f3' : '#e0e0e0' }}>
                            <Typography variant="caption" sx={{ fontWeight: 'bold', color: message.sender === userDetails.username ? '#ffffff' : '#000000' }}>
                                {message.private ? `@${message.sender}` : message.sender}
                            </Typography>
                        </Box>
                    </Box>
                    <Typography variant="body1">{message.content}</Typography>
                </Box>
            </Box>
        ));
    };





    const renderLoggedInContent = () => (
        <Box p={2}>
            <TextField
                fullWidth
                value={newMessage}
                onChange={(e) => setNewMessage(e.target.value)}
                variant="outlined"
                InputProps={{ sx: { borderRadius: 0 } }}
            />
            <Button onClick={sendMessage} variant="contained" sx={{ borderRadius: 0, marginTop: '8px' }}>Envoyer</Button>
        </Box>
    );


    const renderLoggedOutContent = () => (
        <Box p={2}>
            <Typography variant="body1" color="error">Veuillez vous connecter pour envoyer des messages.</Typography>
        </Box>
    );

    const renderConnectedUsers = () => (
        <Box p={2} mt="auto">
            <Typography variant="h6">Utilisateurs connectés :</Typography>
            <ul style={{ listStyleType: 'none', padding: 0 }}>
                {connectedUsers.map((user, index) => (
                    <li key={index} style={{ color: user === 'admin' ? 'white' : getRandomColor(), borderRadius: 5, padding: '4px', display: 'inline-block', backgroundColor: user === 'admin' ? 'red' : '#e0e0e0' }}>
                        {user}
                    </li>
                ))}
            </ul>
        </Box>
    );


    return (
        <Box display="flex" flexDirection="row" height="93vh">
            <Box display="flex" flexDirection="column" flexGrow={1}>
                <Box position="relative" zIndex={1} p={3}>
                    <Box position="absolute" right="10px" top="10px" transform="translate(-50%, 0)">
                        <Box position="absolute" right="10px" top="10px" display="flex" alignItems="center" justifyContent="center" gap="8px">
                            <Button
                                onClick={() => joinRoom('general')}
                                variant="contained"
                                sx={{
                                    bgcolor: currentRoom === 'general' ? 'green' : 'darkgray',
                                    color: 'white',
                                    '&:hover': {
                                        bgcolor: currentRoom === 'general' ? 'darkgreen' : 'gray'
                                    }
                                }}
                            >
                                Room générale
                            </Button>
                            <Button
                                onClick={() => joinRoom('salle1')}
                                variant="contained"
                                sx={{
                                    bgcolor: currentRoom === 'salle1' ? 'green' : 'darkgray',
                                    color: 'white',
                                    '&:hover': {
                                        bgcolor: currentRoom === 'salle1' ? 'darkgreen' : 'gray'
                                    }
                                }}
                            >
                                Salle 1
                            </Button>
                        </Box>
                    </Box>
                </Box>
                <Box
                    position="absolute"
                    right="10px"
                    bottom="10px"
                    display="flex"
                    alignItems="center"
                    padding="8px 20px 8px 10px"
                    borderRadius="20px"
                    backgroundColor="#EEEEEE"
                >
                    <Switch
                        checked={scrollEnabled}
                        onChange={toggleScroll}
                        color="default"
                        inputProps={{ 'aria-label': 'controlled' }}
                        sx={{
                            marginRight: '8px',
                            '& .MuiSwitch-track': {
                                backgroundColor: scrollEnabled ? '#4CAF50' : '#F44336',
                            },
                            '& .MuiSwitch-thumb': {
                                backgroundColor: '#FFFFFF',
                            },
                        }}
                    />
                    <Typography variant="body2" sx={{ color: scrollEnabled ? '#4CAF50' : '#F44336', fontSize: '1.1em', fontWeight: 'bold' }}>
                        {scrollEnabled ? 'Scroll automatique actif' : 'Scroll automatique désactivé'}
                    </Typography>
                </Box>

                <Box flexGrow={1} p={2} style={{ overflowY: 'auto' }}>
                    {renderMessages()}
                    <div ref={messagesEndRef} />
                </Box>

                {isLoggedIn ? renderLoggedInContent() : renderLoggedOutContent()}

                {renderConnectedUsers()}
            </Box>
        </Box>
    );
};
