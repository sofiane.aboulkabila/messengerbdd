import { useNavigate } from 'react-router-dom';
import Button from '@mui/material/Button';
import { Modal_Interface } from '../components/Modal/Modal_Interface';
import { useAuth } from '../security/Auth';

export const UserPage = () => {
    const { modals, toggleModal, modalComponents } = Modal_Interface();
    const navigateTo = useNavigate();
    const { userDetails } = useAuth();

    return (
        <div className="UserPage">
            {Object.keys(modals).map(key => {
                const ModalComponent = modalComponents[key];
                return (
                    <ModalComponent
                        key={key}
                        open={modals[key]}
                        handleClose={() => toggleModal(key)}
                    />
                );
            })}

            <div className="UserPageChild1">
                <div className="UserPage-profil-card Card">
                    <div className="UserPage-profil-card-img Card-img">
                        <img src="./img/default-user-img.png" alt="Profile Photo" />
                    </div>
                    <Button variant="contained" className="UserPage-profil-card-button UserBtn"
                        onClick={() => toggleModal('img')}>Changer la photo de profil</Button>
                </div>

                <div className="UserPage-color-card Card">
                    <div className="UserPage-color-card-img Card-img">
                        <img src="./img/palette.png" alt="Color Palette" />
                    </div>
                    <Button variant="contained" className="UserPage-color-card-button UserBtn"
                        onClick={() => toggleModal('color')}>Acheter le changement de couleur</Button>
                </div>
                <div className="UserPage-pseudo-card Card">
                    <div className="UserPage-pseudo-card-titre Title-card">
                        <h2 className='Title-of-pseudo-card'>{userDetails.username}</h2>
                    </div>
                    <div className='Buttons-UserPage'>
                        <Button variant="contained" className="UserPage-pseudo-card-button1"
                            onClick={() => toggleModal('pseudo')}>Changer le pseudo</Button>
                        <Button variant="contained" className="UserPage-pseudo-card-button2"
                            onClick={() => toggleModal('password')}>Changer le mot de passe</Button>
                    </div>
                </div>
                <div className="UserPage-salons-card Card">
                    <div className="UserPage-salons-card-titre Title-card">
                        <h2 className='Title-of-salons-card'>0 / 5 Salons privés</h2>
                    </div>
                    <Button variant="contained" className="UserPage-salons-card-button"
                        onClick={() => navigateTo('/shop')}>Acheter plus</Button>
                </div>
            </div>

            <div className="UserPageChild2">
                <div className="UserPage-block-card">
                    <div className="UserPage-block-card-titre">
                        <h1>Blocked User List</h1>
                    </div>
                    <ul className="UserPage-block-card-ul">
                        <li className="UserPage-block-card-li">
                            <h3>Username1</h3>
                            <Button variant="outlined" color="error" className="UserPage-block-card-button"
                                onClick={() => toggleModal('confirm')}>Unblock</Button>
                        </li>
                        <li className="UserPage-block-card-li">
                            <h3>Username2</h3>
                            <Button variant="outlined" color="error" className="UserPage-block-card-button"
                                onClick={() => toggleModal('confirm')}>Unblock</Button>
                        </li>
                        <li className="UserPage-block-card-li">
                            <h3>Username3</h3>
                            <Button variant="outlined" color="error" className="UserPage-block-card-button"
                                onClick={() => toggleModal('confirm')}>Unblock</Button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};
