import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Link, useNavigate } from 'react-router-dom';
import { useAuth } from '../security/Auth';
import CircularProgress from '@mui/material/CircularProgress';

export const Navbar = () => {
    const navigate = useNavigate();
    const { isLoggedIn, logout, loading } = useAuth();
    const handleLogout = async () => {
        await logout();
        navigate('/');
    };


    return (
        <AppBar position="static" sx={{ backgroundColor: '#443f40' }}>
            <Toolbar>
                <Box sx={{ flexGrow: 1, display: 'flex', gap: 3 }}>
                    <Link to="/" style={{ textDecoration: 'none', color: 'inherit' }}>
                        <Typography variant="h6">Tchat</Typography>
                    </Link>
                </Box>
                {loading ? (
                    <CircularProgress color="inherit" />
                ) : isLoggedIn ? (
                    <>
                        <Link to="/user" style={{ textDecoration: 'none', color: 'inherit', flexGrow: 0.02 }}>
                            <Typography variant="h6">Profil</Typography>
                        </Link>
                        <Link to="/shop" style={{ textDecoration: 'none', color: 'inherit', flexGrow: 0.02 }}>
                            <Typography variant="h6">Boutique</Typography>
                        </Link>
                        <Button onClick={handleLogout} variant="contained" color="error">Se déconnecter</Button>
                    </>
                ) : (
                    <>
                        <Button onClick={() => navigate('/login')} variant="contained" color="primary" sx={{ marginRight: 2 }}>Se connecter</Button>
                        <Button onClick={() => navigate('/register')} variant="contained" color="secondary">S'inscrire</Button>
                    </>
                )}
            </Toolbar>
        </AppBar>
    );
};
