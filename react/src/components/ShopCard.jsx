import { Box, Typography, Button } from '@mui/material';
import { Modal_Interface } from './Modal/Modal_Interface';

export const ShopCard = ({ title, description, price, className }) => {
    const { modals, toggleModal, modalComponents } = Modal_Interface();

    return (
        <Box className={`ShopPage-card ${className}`}>
            {Object.keys(modals).map(key => {
                const ModalComponent = modalComponents[key];
                return (
                    <ModalComponent
                        key={key}
                        open={modals[key]}
                        handleClose={() => toggleModal(key)}
                        price={price}
                    />
                );
            })}

            <Typography variant="h6" component="p" className="ShopPage-card-header">
                {title}
            </Typography>
            <Box className="ShopPage-card-img" ></Box>
            <Typography className="ShopPage-card-description">
                {description}
            </Typography>
            <Button variant="contained" className="ShopPage-card-button"
                onClick={() => toggleModal('shop')}>
                Acheter à {price}
            </Button>

        </Box>
    );
};
