
import { ModalBox } from './ModalBox';
import { UserPasswordForm } from '../../form/User/UserPasswordForm';

export const ModalUserProfil_Password = ({ open, handleClose }) => {
    return (
        <ModalBox
            modalContent={<UserPasswordForm />}
            open={open}
            handleClose={handleClose}
        />
    );
};
