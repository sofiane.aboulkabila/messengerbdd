
import { ModalBox } from './ModalBox';

export const ModalUserProfil_Img = ({ open, handleClose }) => {
    return (
        <ModalBox
            modalContent={'Img'}
            open={open}
            handleClose={handleClose}
        />
    );
};
