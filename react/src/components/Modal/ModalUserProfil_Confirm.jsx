import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

export const ModalUserProfil_Confirm = ({ open, handleClose, userName }) => {
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box
                sx={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    bgcolor: 'background.paper',
                    boxShadow: 24,
                    p: 4,
                    maxWidth: 400,
                    borderRadius: 2
                }}
            >
                <Typography id="modal-modal-title" variant="h6" component="h2" align="center">
                    Êtes-vous sûr de vouloir débloquer {userName} ?
                </Typography>
                <Box sx={{ display: 'flex', justifyContent: 'flex-end', mt: 2 }}>
                    <Button color="primary" variant="outlined" sx={{ mr: 1 }}>
                        Annuler
                    </Button>
                    <Button color="primary" variant="contained">
                        Valider
                    </Button>
                </Box>
            </Box>
        </Modal>
    );
};