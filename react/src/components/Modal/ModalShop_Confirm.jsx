
import { ModalBox } from './ModalBox';

export const ModalShop_Confirm = ({ open, handleClose, price }) => {
    return (
        <ModalBox
            modalContent={`Confirmer l'achat à ${price}`}
            open={open}
            handleClose={handleClose}
        />
    );
};