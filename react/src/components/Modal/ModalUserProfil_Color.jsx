
import { ModalBox } from './ModalBox';
import { UserColorForm } from '../../form/User/UserColorForm';

export const ModalUserProfil_Color = ({ open, handleClose }) => {
    return (
        <ModalBox
            modalContent={<UserColorForm />}
            open={open}
            handleClose={handleClose}
        />
    );
};
