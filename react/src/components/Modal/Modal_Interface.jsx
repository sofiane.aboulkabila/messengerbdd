import { useState, useCallback } from 'react';
import { ModalUserProfil_Color } from './ModalUserProfil_Color';
import { ModalUserProfil_Img } from './ModalUserProfil_Img';
import { ModalUserProfil_Confirm } from './ModalUserProfil_Confirm';
import { ModalUserProfil_Password } from './ModalUserProfil_Password';
import { ModalUserProfil_Pseudo } from './ModalUserProfil_Pseudo';
import { ModalShop_Confirm } from './ModalShop_Confirm';

export const Modal_Interface = () => {
    const [modals, setModals] = useState({
        color: false,
        img: false,
        confirm: false,
        password: false,
        pseudo: false,
        shop: false
    });

    const modalComponents = {
        color: ModalUserProfil_Color,
        img: ModalUserProfil_Img,
        confirm: ModalUserProfil_Confirm,
        password: ModalUserProfil_Password,
        pseudo: ModalUserProfil_Pseudo,
        shop: ModalShop_Confirm
    };

    const toggleModal = useCallback((modalKey) => {
        setModals(prev => ({ ...prev, [modalKey]: !prev[modalKey] }));
    }, []);

    return { modals, toggleModal, modalComponents };
};