
import { ModalBox } from './ModalBox';
import { UserPseudoForm } from '../../form/User/UserPseudoForm';

export const ModalUserProfil_Pseudo = ({ open, handleClose }) => {
    return (
        <ModalBox
            modalContent={<UserPseudoForm />}
            open={open}
            handleClose={handleClose}
        />
    );
};
