import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';

export const ModalBox = ({
    open,
    handleClose,
    modalContent,
}) => {

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-describedby="modal-content"
        >
            <Box sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                maxWidth: '90%',
                maxHeight: '90vh',
                overflowY: 'auto',
                backgroundColor: '#ffffff',
                borderRadius: '10px',
                border: '1px solid #3f424975',
                boxShadow: '0px 4px 8px rgba(70, 68, 68, 0.1)',
                outline: 'none',
                padding: '20px'
            }}>
                <Box id="modal-content" className="modal-content">
                    {modalContent}
                </Box>
            </Box>
        </Modal >
    );
};
