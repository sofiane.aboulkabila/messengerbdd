const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const cors = require('cors');

const app = express();
app.use(cors());

const server = http.createServer(app);

const io = new Server(server, {
    cors: {
        origin: 'http://localhost:5173',
        methods: ['GET', 'POST']
    }
});

let connectedUsers = {};

io.on("connection", (socket) => {
    socket.on('send-nickname', (username) => {
        connectedUsers[socket.id] = username;
        io.emit('connected_users', Object.values(connectedUsers));
        socket.join("general");
        io.to("general").emit("receive_message", {
            sender: 'System',
            content: `Bienvenue ${username} dans le salon général!`
        });
    });

    socket.on('disconnect', () => {
        delete connectedUsers[socket.id];
        io.emit('connected_users', Object.values(connectedUsers));
    });

    socket.on("join_room", (room) => {
        const oldRoom = Array.from(socket.rooms).find(r => r !== socket.id && r !== room);
        if (oldRoom) {
            socket.leave(oldRoom);
        }
        socket.join(room);
        if (room !== oldRoom) {
            io.to(room).emit("receive_message", {
                sender: 'System',
                content: `${connectedUsers[socket.id]} a rejoint le salon ${room}.`
            });
        }
    });

    socket.on("send_message", (data) => {
        const message = { sender: data.sender, content: data.message };
        io.to(data.room).emit("receive_message", message);
    });

    socket.on("send_private_message", (data) => {
        const senderUsername = connectedUsers[socket.id];
        const recipientSocketId = Object.keys(connectedUsers).find(key => connectedUsers[key] === data.recipient);
        if (recipientSocketId) {
            const message = { sender: senderUsername, content: data.message, private: true };
            io.to(recipientSocketId).emit("receive_message", message);
        } else {
            console.log(`Utilisateur ${data.recipient} non trouvé.`);
        }
    });

});

server.listen(3003, () => {
    console.log("Socket.io server listening on port 3003 !");
});
